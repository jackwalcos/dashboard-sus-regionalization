import os
import math
import statistics

import dash
import dash_deck
from dash import Input, Output, State, html
from dash import dcc
import dash_bootstrap_components as dbc
import pydeck as pdk
import plotly.express as px
import pandas as pd

from components.patient_flow_tab import tab_flow
from components.detected_communities_tab import tab_communities
from components.selfsufficiency_tab import tab_selfsufficiency
from helpers import (
    process_flow_data, process_hosp_data, get_diagnosis, get_state_latlong, get_items_level,
    get_shape_layer, get_communities_layer, get_map_layer, get_mini_comm_maps, get_self_layer, 
    get_mini_self_maps, get_state_layer, 
)

mapbox_api_token = os.getenv("MAPBOX_ACCESS_TOKEN")

# CREATE APP
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])
server = app.server


# CREATE COMPONENTS
dropdown_uf = dcc.Dropdown(
   options=[
       {'label': 'Rio de Janeiro', 'value': 'Rio de Janeiro'},
    ],
   value='Rio de Janeiro',
   searchable=True,
   clearable=False,
   id="uf_drop",
)

dropdown_diag = dcc.Dropdown(
   options=get_diagnosis(),
   value='all',
   searchable=True,
   clearable=False,
   id="diag_drop",
)

dropdown_level = dcc.Dropdown(
   options=[
       {'label': 'Municípios', 'value': 'mun'},
       {'label': 'Regiões de Saúde', 'value': 'reg'},
       {'label': 'Macrorregiões de Saúde', 'value': 'macro'},
    ],
   value='mun',
   clearable=False,
   id="level_drop",
)

tabs = dbc.Tabs(
    [
        dbc.Tab(tab_flow(app), label="Fluxo de pacientes", tab_id="tab1"),
        dbc.Tab(tab_communities(app), label="Divisões sugeridas", tab_id="tab2"),
        dbc.Tab(tab_selfsufficiency(app), label="Autossuficiência estimada", tab_id="tab3"),
    ], active_tab="tab1",
    className="justify-content-center nav-fill", id="tabs_id"
)

app.layout = html.Div(
    className="container-fluid text-white bg-dark full-height",
    children=[
        html.Br(),
        dbc.Row(
            [
                dbc.Col(html.H2(children="Dashboard Regionalização SUS", className="display-5"), width=6),
                dbc.Col(["Escolha o estado:", dropdown_uf], width=2),
                dbc.Col(["Diagnóstico:", dropdown_diag], width=2),
                dbc.Col(["Nível de aproximação:", dropdown_level], width=2),
            ]
        ),
        tabs,
    ],
)


# CREATE CALLBACK FUNCTIONS
#   TAB1 CALLBACKS
@app.callback(Output("legend", "style"),
              Input("map_radios", "value"))
def switch_legend(ratio_value):
    if(ratio_value=='flow'):
        return {'display': 'block',
                'position': 'absolute',
                'bottom': '60px',
                'right': '35px'}
    else: return {'display': 'None'}

@app.callback([Output("diagnosis_span_flow", "children"),
               Output("year_span_flow", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_flow", "value")])
def get_diagnosis_name_flow(diag_value, year):
    return " | " + get_diagnosis(diag_value), year

@app.callback([Output("choose_bar_level", "children"),
               Output("dropdown_bar_level", "children")], 
              Input("level_drop", "value"))
def display_bar_charts(level_value):
    options = get_items_level(level_value)
    dropdown = dcc.Dropdown(
        options=options,
        value=options[0],
        searchable=True,
        clearable=False,
        id="level_items_drop",
    )
    
    if(level_value=='reg'): return "Escolha a região de saúde:", dropdown
    if(level_value=='macro'): return "Escolha a macrorregião de saúde:", dropdown
    return "Escolha a cidade:", dropdown

@app.callback([Output("top5_inflow_output", "children"),
               Output("top5_outflow_output", "children")], 
              [Input("year_slider_flow", "value"),
               Input("diag_drop", "value"),
               Input("level_drop", "value"),
               Input("level_items_drop", "value")])
def display_top5(year, diag_value, level_value, level_items):
    # IMPORT AND PROCESS DATA
    if(diag_value == "all"):
        df = pd.read_csv("./data/csv/%s_sih_from_to_flow.csv"%level_value)
    else:
        df = pd.read_csv("./data/csv/diagnosis/%s/%s_sih_from_to_flow.csv"%(level_value, diag_value))

    df = process_flow_data(df, int(year))[['from_name', 'to_name', 'hospitalizations']]

    df_inflow = df[df['to_name']==level_items].sort_values(by='hospitalizations', ascending=False).head(5)
    df_outflow = df[df['from_name']==level_items].sort_values(by='hospitalizations', ascending=False).head(5)


    others_sum = df[df['to_name']==level_items]['hospitalizations'].sum() - df_inflow['hospitalizations'].sum()
    others = pd.DataFrame([['Outros', 'Outros', others_sum]],
                   columns=['from_name', 'to_name', 'hospitalizations'])
    if(others_sum > 0): df_inflow = pd.concat([df_inflow, others])

    others_sum = df[df['from_name']==level_items]['hospitalizations'].sum() - df_outflow['hospitalizations'].sum()
    others = pd.DataFrame([['Outros', 'Outros', others_sum]],
                   columns=['from_name', 'to_name', 'hospitalizations'])
    if(others_sum > 0): df_outflow = pd.concat([df_outflow, others])

    bar_chart_inflow = dcc.Graph(
        figure=px.bar(
            df_inflow.iloc[::-1], title="Internações recebidas em %s"%level_items,
            x="hospitalizations", y="from_name", orientation='h', height=230, text_auto=True,
            color_discrete_sequence=px.colors.qualitative.Pastel[1:], template="plotly_dark", 
            labels={'hospitalizations':'Internações', 'from_name':'Origem'},
        ).update_layout(margin={"r":0,"t":46,"l":0,"b":6}).update_xaxes(visible=False).update_xaxes(title_text='Internações')
    )

    bar_chart_outflow = dcc.Graph(
        figure=px.bar(
            df_outflow.iloc[::-1], title="Internações que partiram de %s"%level_items,
            x="hospitalizations", y="to_name", orientation='h', height=230, text_auto=True,
            color_discrete_sequence=px.colors.qualitative.Pastel, template="plotly_dark", 
            labels={'hospitalizations':'Internações', 'to_name':'Destino'},
        ).update_layout(margin={"r":0,"t":46,"l":0,"b":6}).update_xaxes(visible=False)
    )

    return bar_chart_inflow, bar_chart_outflow

@app.callback(Output("map_output_flow", "children"), 
             [Input("uf_drop", "value"),
             Input("year_slider_flow", "value"),
             Input("diag_drop", "value"), 
             Input("level_drop", "value"), 
             Input("map_radios", "value"),
             Input("tabs_id", "active_tab")])
def display_flow_map(uf_name, year, diag_value, level_value, ratio_value, tab_value):
    # IMPORT AND PROCESS DATA
    if(diag_value == "all"):
        df = pd.read_csv("./data/csv/%s_sih_from_to_flow.csv"%level_value)
    else:
        df = pd.read_csv("./data/csv/diagnosis/%s/%s_sih_from_to_flow.csv"%(level_value, diag_value))

    df = process_flow_data(df, int(year))

    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(level_value)

    # CREATE MAP LAYER
    if(ratio_value == "flow"): layer, tooltip = get_map_layer("flow", df)
    else: layer, tooltip = get_map_layer("scatter", process_hosp_data(df))

    # Define the map center based on shape
    lat, lon = get_state_latlong(uf_name)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=7,
    )

    layers = [shape_layer, layer]
    r = pdk.Deck(layers, initial_view_state=view_state, mapbox_key=mapbox_api_token)

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container


#   TAB2 CALLBACKS
@app.callback(Output("legend_comm", "style"),
              Input("tabs_id", "active_tab"))
def display_comm_legend(tab_value):
    if(tab_value=='tab2'):
        return {'display': 'block',
                'position': 'absolute',
                'bottom': '60px',
                'right': '35px'}
    else: return {'display': 'None'}

@app.callback([Output("level_drop", "options"),
               Output("level_drop", "value")],
              Input("tabs_id", "active_tab"),
              State("level_drop", "value"))
def change_level_options(tab_value, level_state):
    value = level_state

    if(tab_value=='tab2' or tab_value=='tab3'):
        if(level_state=='mun'): value = 'reg'

        return [{'label': 'Regiões de Saúde', 'value': 'reg'},
                {'label': 'Macrorregiões de Saúde', 'value': 'macro'}], value
    return [
            {'label': 'Municípios', 'value': 'mun'},
            {'label': 'Regiões de Saúde', 'value': 'reg'},
            {'label': 'Macrorregiões de Saúde', 'value': 'macro'}], value

@app.callback([Output("diagnosis_span_comm", "children"),
               Output("year_span_comm", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_comm", "value")])
def get_diagnosis_name_comm(diag_value, year):
    return " | " + get_diagnosis(diag_value), year

@app.callback([Output("map_output_comm", "children"),
              Output("mini_shape_reg", "children"),
              Output("mini_shape_comm", "children")],
             [Input("uf_drop", "value"),
              Input("year_slider_comm", "value"),
              Input("diag_drop", "value"), 
              Input("level_drop", "value"),
              Input("tabs_id", "active_tab")])
def display_comm_map(uf_name, year, diag_value, level_value, tab_value):
    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(level_value, 'blue')

    # CREATE COMMUNITIES LAYER
    comm_layer = get_communities_layer(diag_value, level_value, year)
    
    # Define the map center based on shape
    lat, lon = get_state_latlong(uf_name)

    mini_deck1, mini_deck2 = get_mini_comm_maps(diag_value, level_value, year, lat, lon)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=7,
    )

    layers = [shape_layer, comm_layer, get_state_layer()]
    r = pdk.Deck(layers, initial_view_state=view_state, mapbox_key=mapbox_api_token)

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            #tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container, mini_deck1, mini_deck2

@app.callback([Output("line_chart_output_comm", "children"),
               Output("similarity_level", "children")],
              [Input("year_slider_comm", "value"),
               Input("diag_drop", "value"),
               Input("level_drop", "value")])
def display_similarity_comm(year, diag_value, level_value):
    if(level_value=='mun'): return html.Div(), html.Div()
    df = pd.read_csv("./data/csv/similarity_%s.csv"%level_value)

    line_chart_similarity = dcc.Graph(
        figure=px.line(df, x='year', y=diag_value, #color='country', 
                        labels={'year':'Ano', diag_value:'Similaridade (%)'},
                        markers=True, height=227, template="plotly_dark", 
                        color_discrete_sequence=px.colors.sequential.Greens[4:])
        .update_layout(margin={"r":5,"t":5,"l":5,"b":5}).update_yaxes(range=[0, 100])#.update_xaxes(visible=False)
        .add_shape(dict(type="line", x0=year, y0=0, x1=year, y1=100,
                        line=dict(color="white", width=3, dash="dot"))
        )
    )
    return line_chart_similarity, "%.1f%%"%df[df['year']==year][diag_value]


#   TAB3 CALLBACKS
@app.callback([Output("diagnosis_span_self", "children"),
               Output("year_span_self", "children")], 
              [Input("diag_drop", "value"),
               Input("year_slider_self", "value")])
def get_diagnosis_name_self(diag_value, year):
    #return " | " + get_diagnosis(diag_value), year
    return "", year

@app.callback([Output("map_output_self", "children"),
               Output("mini_lofi", "children"),
               Output("mini_1-lifo", "children")],
             [Input("uf_drop", "value"),
              Input("year_slider_self", "value"),
              Input("diag_drop", "value"), 
              Input("level_drop", "value"),
              Input("atracao_slider_self", "value")])
def display_self_map(uf_name, year, diag_value, level_value, beta):
    # CREATE SHAPE LAYER
    self_layer = get_self_layer(year, diag_value, level_value, beta=beta)

    # Define the map center based on shape
    lat, lon = get_state_latlong(uf_name)

    mini1, mini2 = get_mini_self_maps(diag_value, level_value, year, lat, lon)

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=7,
    )

    r = pdk.Deck(self_layer, initial_view_state=view_state, mapbox_key=mapbox_api_token)
    tooltip = {"html": "<b>{name}</b> <br /><b>Autossuficiência:</b> {hmean}"}

    deck_container = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl", 
            tooltip=tooltip, 
            mapboxKey=r.mapbox_key
        ),
        style={"height": "550px", "width": "100%", "position": "relative"},
    )
    return deck_container, mini1, mini2


if __name__ == "__main__":
    app.run_server(debug=True)