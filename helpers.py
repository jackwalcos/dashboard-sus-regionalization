import math
import statistics

import pandas as pd
import geopandas as gpd
import numpy as np
import pydeck as pdk
import dash_deck
from dash import html
from shapely.ops import cascaded_union

def get_diagnosis(diag='entire'):
    diag_dict = {
        'all': 'Todos os diagnósticos',
        'neoplasm': 'Neoplasias',
        'circulatory_system': 'Doenças do aparelho circulatório',
        'parasitic_diseases': 'Doenças parasitárias',
        'musculoskeletal_system': 'Doenças do sistema osteomuscular',
        'external_causes': 'Causas externas',

        'eye_ear_diseases': 'Doenças do olho e ouvido',
        'nervous_system': 'Doenças do sistema nervoso',
        'genitourinary_system': 'Doenças do aparelho geniturinário',

        'pregnancy': 'Gravidez',
        'diabetes': 'Diabetes',
        'mental_disorders': 'Transtornos mentais',
        'skin_diseases': 'Doenças da pele'
    }
    if(diag == 'entire'): return diag_dict
    else: return diag_dict[diag]

def get_items_level(level_value):
    df = pd.read_csv("./data/csv/%s_sih_from_to_flow.csv"%level_value)
    df['from'] = df['from'].map(eval)
    df['from_name'] = df["from"].apply(lambda f: f["name"])

    return np.unique(df['from_name'])

def process_flow_data(df, year):
    df = df[df['year'] == year].copy()

    # Evaluate dictionary parts
    df['from'] = df['from'].map(eval)
    df['to'] = df['to'].map(eval)

    # Prepare data for tooltip
    df["from_name"] = df["from"].apply(lambda f: f["name"])
    df["to_name"] = df["to"].apply(lambda t: t["name"])
    # Calculate additional data
    df["hospitalizations_rate"] = df["hospitalizations"].apply(lambda t: t*0.005)

    # Normalize and weight flow layer height to avoid z-fighting
    hosp_min, hosp_max = np.min(df["hospitalizations"]), np.max(df["hospitalizations"])
    df["height_rate"] = df["hospitalizations"].apply(lambda t: 0.3*(1-((t-hosp_min)/(hosp_max-hosp_min))))

    return df

def process_hosp_data(df):
    hosp_to = df[['to_name', 'to', 'hospitalizations']].copy()
    hosp_to['to'] = hosp_to['to'].map(str)
    # Get the number of hospitalizations each municipality had
    hosp_to = hosp_to.groupby(by = ['to_name', 'to'], as_index = False).sum()
    # Calculate additional data
    hosp_to["hospitalizations_radius"] = hosp_to["hospitalizations"].apply(lambda hospitalizations: 10*math.sqrt(hospitalizations))
    hosp_to['to'] = hosp_to['to'].map(eval)

    return hosp_to

def get_state_latlong(uf_name):
    state_center = {
        "Rio de Janeiro": [-22.19756813181387, -42.66442880031974]
    }

    return state_center[uf_name]

def get_latlong_mean(df):
    lat, lon = [], []
    df.apply(lambda t: lat.append(t['latlong'][0]))
    df.apply(lambda t: lon.append(t['latlong'][1]))

    return round(statistics.mean(lon), 1), round(statistics.mean(lat), 1)

def get_shape_layer(level_type, shape_type='gray', shape='default'):
    if(level_type=='mun'): gdf = gpd.read_file("./data/shapes/state.shp")
    elif(level_type=='reg'): gdf = gpd.read_file("./data/shapes/regions_2017.shp")
    elif(level_type=='macro'): gdf = gpd.read_file("./data/shapes/macroregions.shp")

    if(shape_type=='gray'): line_width, color = 1, [150, 150, 150]
    elif(shape_type=='blue'): line_width, color = 5, [6, 150, 187, 100]

    if(shape=='mini'): line_width = 2

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=line_width,
        filled=True,
        get_line_color=color,
        get_fill_color=[150, 150, 150, 15],
    )
    #center = cascaded_union(gdf['geometry'].values).centroid.xy
    #print(center)

    return layer

def get_communities_layer(diag_type, level_type, year, shape='default'):
    if(diag_type=='all'): 
        if(level_type=='macro'): gdf = gpd.read_file("./data/shapes/communities/macro_comm_%d.shp"%year)
        else: gdf = gpd.read_file("./data/shapes/communities/comm_%d.shp"%year)
    else:
        if(level_type=='macro'): gdf = gpd.read_file("./data/shapes/communities/diagnosis/%s_macro_comm_%d.shp"%(diag_type, year))
        else: gdf = gpd.read_file("./data/shapes/communities/diagnosis/%s_comm_%d.shp"%(diag_type, year))
    

    if(shape=='mini'): line_width = 2
    else: line_width = 5

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=line_width,
        filled=True,
        get_line_color=[253, 198, 10, 50],
        get_fill_color=[150, 150, 150, 15],
    )

    return layer

def get_state_layer():
    gdf = gpd.read_file("./data/shapes/state.shp")

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=5,
        filled=False,
        get_line_color=[97, 136, 85],
    )

    return layer

def get_map_layer(layer_type, df):
    if(layer_type=="flow"):
        layer = pdk.Layer(
            "GreatCircleLayer",
            df,
            pickable=True,
            getHeight="height_rate",
            getWidth="hospitalizations_rate",
            get_source_position="from.latlong",
            get_target_position="to.latlong",
            get_source_color=[0, 128, 200],
            get_target_color=[200, 128, 0],
            auto_highlight=True,
        )
        tooltip = {"text": "{from_name} para {to_name}\n{hospitalizations} internações"}
    else:
        layer = pdk.Layer(
            "ScatterplotLayer",
            df,
            pickable=True,
            opacity=0.8,
            stroked=True,
            filled=True,
            radius_scale=6,
            radius_min_pixels=1,
            radius_max_pixels=100,
            line_width_min_pixels=1,
            get_position="to.latlong",
            get_radius="hospitalizations_radius",
            get_fill_color=[200, 128, 0],
            get_line_color=[0, 0, 0],
        )
        tooltip = {"text": "{to_name}\n{hospitalizations} internações realizadas"}
    
    return layer, tooltip

def get_mini_comm_maps(diag_value, level_value, year, lat, lon):
    # CREATE SHAPE LAYER
    shape_layer = get_shape_layer(level_value, 'blue', shape='mini')

    # CREATE COMMUNITIES LAYER
    comm_layer = get_communities_layer(diag_value, level_value, year, shape='mini')
    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=5.3,
    )

    r = pdk.Deck(shape_layer, initial_view_state=view_state)
    container1 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
        ),
        style={"height": "170px", "width": "100%", "position": "relative"},
    )

    r = pdk.Deck(comm_layer, initial_view_state=view_state)
    container2 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
        ),
        style={"height": "170px", "width": "100%", "position": "relative"},
    )

    return container1, container2

def color_scale(index):
    colors=[[194, 96, 4, 150],
            [210, 124, 61, 150],
            [222, 153, 105, 150],
            [232, 182, 149, 150],
            [238, 211, 194, 150],
            [198, 207, 215, 150],
            [155, 175, 190, 150],
            [113, 144, 165, 150],
            [70, 114, 141, 150],
            [4, 85, 117, 150]]
            
    return colors[int(index*10//1)]

def to_1_lifo(x):
    return 1-x

def lifo_lofi_hmean(mlifo, lofi, w_mlifo=0.5, w_lofi=1):
    return (w_mlifo+w_lofi)/((w_mlifo/mlifo)+(w_lofi/lofi))

def get_self_layer(year, diag_value, level_value, value="hmean", beta=0.5):
    if(level_value=='reg'): gdf = gpd.read_file("./data/shapes/regions_2017.shp")
    elif(level_value=='macro'): gdf = gpd.read_file("./data/shapes/macroregions.shp")
    else: return ''

    df = pd.read_csv("./data/csv/selfsufficiency_%s.csv"%level_value)
    df = df[df['year']==year]

    if(value=='hmean'): df["hmean"] = lifo_lofi_hmean(to_1_lifo(df['lifo']), df['lofi'], w_mlifo=beta)
    elif(value=='lifo'): 
        df['1-lifo'] = to_1_lifo(df['lifo'])
        value = '1-lifo'

    gdf = gdf.merge(df[['name', value]], how='left', left_on=['name'], right_on=['name'])
    gdf['color'] = gdf[value].map(lambda i: color_scale(i))

    layer = pdk.Layer(
        "GeoJsonLayer",
        gdf,
        stroked=True,
        lineWidthMinPixels=2,
        filled=True,
        get_fill_color="color",
        pickable=True,
        auto_highlight=True,
    )

    return layer

def get_mini_self_maps(diag_value, level_value, year, lat, lon):
    lofi_layer = get_self_layer(year, diag_value, level_value, "lofi")
    lifo_layer = get_self_layer(year, diag_value, level_value, "lifo")

    view_state = pdk.ViewState(
        latitude=lat, longitude=lon, 
        bearing=0, pitch=0, zoom=5.3,
    )

    r = pdk.Deck(lofi_layer, initial_view_state=view_state)
    container1 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
            tooltip = {"html": "<b>{name}</b> <br /><b>Taxa de permanência:</b> {lofi}"}
        ),
        style={"height": "160px", "width": "100%", "position": "relative"},
    )

    r = pdk.Deck(lifo_layer, initial_view_state=view_state)
    container2 = html.Div(
        dash_deck.DeckGL(
            r.to_json(), 
            id="deck-gl",
            tooltip = {"html": "<b>{name}</b> <br /><b>Taxa de atração:</b> {1-lifo}"}
        ),
        style={"height": "160px", "width": "100%", "position": "relative"},
    )

    return container1, container2
